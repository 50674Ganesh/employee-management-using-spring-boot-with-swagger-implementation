package com.example.employeemanagement.services;

import com.example.employeemanagement.entites.Employee;
import com.example.employeemanagement.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;
  public String addEmployee(Employee employee) {
      employeeRepository.save(employee);
      return "Employee Added Successfully";
  }

  public List <Employee> getAllEmployee () {
      return (List<Employee>) employeeRepository.findAll();
  }

  public String update(Employee employee,int id) {
      Optional<Employee> optional = employeeRepository.findById(id);
      Employee e = optional.get();

      if(e != null){
          e.setName(employee.getName());
          e.setAge(employee.getAge());
          e.setSalary(employee.getSalary());
          e.setEmail(employee.getEmail());
          e.setJoinDate(employee.getJoinDate());
          e.setDepartment(employee.getDepartment());

          employeeRepository.save(e);
          return "Update Employee Successfully";
      }
      else {
          return "Not Employee Update";
      }
  }

  public String deleteById(int id) {
     Optional<Employee> employee= employeeRepository.findById(id);
     Employee e =employee.get();

     if(e != null) {
      employeeRepository.deleteById(id);
      return  "Deleted Employee by Id "+id;
     }
     else
         return "Employee with id"+id+"Not Found";
  }
}
