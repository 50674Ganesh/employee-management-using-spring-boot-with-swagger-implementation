package com.example.employeemanagement.services;

import com.example.employeemanagement.entites.Department;
import com.example.employeemanagement.entites.Employee;
import com.example.employeemanagement.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {

    @Autowired
    public DepartmentRepository departmentRepository;

   public List<Department> getAllDepartment() {
    return (List<Department>)departmentRepository.findAll();
   }

   public String addDepartment(Department department) {
departmentRepository.save(department);
return "Department add to the database";
   }

}
