package com.example.employeemanagement.controllers;

import com.example.employeemanagement.entites.Department;

import com.example.employeemanagement.services.DepartmentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DepartmentController {
     @Autowired
   private DepartmentService departmentService;

    @GetMapping("/departments")
    public List<Department> getAllDepartment() {
     return departmentService.getAllDepartment();

    }

    @PostMapping("/department")
    public String addDepartment(@RequestBody Department department) {
    return  departmentService.addDepartment(department);
    }


}
