package com.example.employeemanagement.controllers;

import com.example.employeemanagement.entites.Employee;
import com.example.employeemanagement.repository.EmployeeRepository;
import com.example.employeemanagement.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;
    @Autowired
    EmployeeRepository employeeRepository;


    @PostMapping("/employee")
    public String addEmployee(@RequestBody Employee employee) {
        return employeeService.addEmployee(employee);
    }

    @GetMapping("/employees")
    public List<Employee> getAllEmployee( ) {
        return employeeService.getAllEmployee();
    }

   @PutMapping("/employee/{id}")
    public String update(@RequestBody Employee employee, @PathVariable int id) {
        return employeeService.update(employee,id);
    }
   @DeleteMapping("/employee/{id}")
    public String deleteById(@PathVariable int id) {
       return employeeService.deleteById(id);
    }

}
